#!/usr/bin/python3

from unittest import main, TestCase, skip
import next_file
import os
import shutil
import logging


class NextFileTests(TestCase):

    _initial_dir = os.getcwd()

    def __init__(self, *args, **kwargs):
        super(NextFileTests, self).__init__(*args, **kwargs)

        next_file.parser = MockedParser()


    def _get_test_method_name(self):
        return self._testMethodName


    @staticmethod
    def get_base_test_directory():
        return os.path.join(NextFileTests._initial_dir, '.__next_file_tests__')


    @staticmethod
    def setUpClass():
        if os.path.exists(NextFileTests.get_base_test_directory()):
            NextFileTests.Helper(None).clean_everything()


    def setUp(self):
        self._helper = self.Helper(self)
        
        self._test_directory = os.path.join(self.get_base_test_directory(), self._get_test_method_name())

        os.makedirs(os.path.join(self._test_directory, 'configs'), exist_ok=True)

        os.chdir(os.path.join(self._test_directory, 'configs'))

        for i in range(1, 6):
            self.Helper.create_empty_file('config_{}'.format(i))


    def tearDown(self):
        global args
        if not args.conserve_test_folder and os.path.exists(self.get_base_test_directory()):
            self.Helper(self).clean_everything()


    class Helper(object):

        def __init__(self, outer):
            self._outer = outer


        @staticmethod
        def create_empty_file(filename):
            open(filename, 'a').close()


        def clean_everything(self):
            shutil.rmtree(NextFileTests.get_base_test_directory())


        def enable_logs(self):
            logging.getLogger().setLevel('DEBUG')


        def disable_logs(self):
            logging.getLogger().setLevel('CRITICAL')


        def remove_all_files_on_configs(self):
            os.chdir('..')
            shutil.rmtree('configs')
            os.mkdir('configs')
            os.chdir('configs')
            

        def create_link_at(self, folder, as_dir=False, as_file=False, last_file=False, broken_link=False):
            if folder == 'parent folder':
                os.chdir('..')

            if as_dir:
                os.mkdir('active')
            elif as_file:
                self.create_empty_file('active')
            else:
                if last_file:
                    dest_file = 'config_5'
                elif broken_link:
                    dest_file = 'batata'
                else:
                    dest_file = 'config_1'

                folder_path = 'configs' if folder == 'parent folder' else ''

                os.symlink(os.path.join(folder_path, dest_file), 'active')

            if folder == 'parent folder':
                os.chdir('configs')

            self._link_already_exists_as_dir = as_dir
            self._link_already_exists_as_file = as_file
            self._link_folder = folder


        def call_from(self, folder, in_context=False):
            self._call_from = folder

            if folder == 'current folder':
                self._link_location = '' 
            elif folder == 'parent folder':
                os.chdir('..')
                self._link_location = 'configs'
            elif folder == 'parent of parent folder':
                os.chdir('../..')
                self._link_location = self._outer._get_test_method_name()
            elif folder == 'anywhere with absolute path':
                os.chdir('/')
                self._link_location = os.path.join(self._outer._test_directory, 'configs')
            else:
                raise Exception('Unable to determine where the next_file should be called')

            self._link_location = os.path.join(self._link_location, 'active')

            if not in_context:
                self._result = next_file.main(link = self._link_location, retrieve_only = False)

            
        def assertThat(self, expected_behaviour):
            if expected_behaviour == 'update with next file':
                updated_link_dest_name = 'config_2'
            elif expected_behaviour == 'link was created with first file':
                updated_link_dest_name = 'config_1'
            elif expected_behaviour == 'update to first file':
                updated_link_dest_name = 'config_1'
            elif expected_behaviour == 'raised parser error':
                with self._outer.assertRaises(MockedParserError, msg='Expected a "MockedParserError" exception....') as context:
                    next_file.main(self._link_location, retrieve_only = False)
             
                if self._link_already_exists_as_dir:
                    object_type = 'directory'
                elif self._link_already_exists_as_file:
                    object_type = 'regular file'

                self._outer.assertEqual(context.exception.message, '"{}" should be a symlink (its a {})'.format(self._link_location, object_type))

                return 
            elif expected_behaviour == 'raised no file to link exception':
                with self._outer.assertRaises(Exception, msg='Expected a Exception due to unable to find a file to link to') as context:
                    next_file.main(self._link_location, retrieve_only = False)

                self._outer.assertEqual(context.exception.args[0], 'Link doesnt exist yet and there are no files to link on')

                return
            else:
                raise Exception('No assertions made')

            if self._call_from == 'current folder':
                updated_link_folder = ''
            elif self._call_from == 'parent folder':
                updated_link_folder = 'configs'
            elif self._call_from == 'parent of parent folder':
                updated_link_folder = os.path.join(self._outer._get_test_method_name(), 'configs')
            elif self._call_from == 'anywhere with absolute path':
                updated_link_folder = os.path.join(self._outer._test_directory, 'configs')

            updated_link = os.path.join(updated_link_folder, updated_link_dest_name)

            self._outer.assertEqual(updated_link, self._result)
            self._outer.assertEqual(updated_link, os.readlink(self._link_location))


    # Variations for each scenario:
    # In current folder (cf)
    # Out current folder (of)
    # Absolute path (ap)
    # Existing link in sub folder which points to subfolder file (sf)


    # Main scenarios

    # Existing link

    def test_passing_existing_local_link_in_current_folder_should_update_local_link_with_next_file(self):
        self._helper.create_link_at('current folder')

        self._helper.call_from('current folder')

        self._helper.assertThat('update with next file')


    def test_passing_pattern_should_ignore_unmatched_files(self):
        self._helper.create_link_at('current folder')

        self._helper.create_empty_file('file_that_should_never_match')

        self._helper.call_from('current folder')
        self._helper.call_from('current folder')
        self._helper.call_from('current folder')
        self._helper.call_from('current folder')
        self._helper.call_from('current folder')

        global _skip_breakpoints
        if not _skip_breakpoints:
          import ipdb; ipdb.set_trace()
        self._helper.assertThat('update to first file')


    def test_passing_existing_relative_link_name_in_sub_folder_should_update_local_link_with_next_file(self):
        self._helper.create_link_at('current folder')

        self._helper.call_from('parent folder')

        self._helper.assertThat('update with next file')


    def test_passing_existing_absolute_link_should_update_link_with_next_file(self):
        self._helper.create_link_at('current folder')

        self._helper.call_from('anywhere with absolute path')

        self._helper.assertThat('update with next file')

 
    def test_passing_existing_relative_link_to_relative_subfolder_should_update_with_next_file(self):
        self._helper.create_link_at('parent folder')

        self._helper.call_from('parent of parent folder')

        self._helper.assertThat('update with next file')


    # When link points to last file in folder should reset to first

    def test_passing_existing_local_link_name_to_last_file_in_folder_should_update_to_first_file(self):
        self._helper.create_link_at('current folder', last_file=True)

        self._helper.call_from('current folder')

        self._helper.assertThat('update to first file')

    
    def test_passing_existing_relative_link_name_to_last_file_in_folder_should_update_to_first_file(self):
        self._helper.create_link_at('current folder', last_file=True)

        self._helper.call_from('parent folder')
    
        self._helper.assertThat('update to first file')


    def test_passing_existing_absolute_link_name_to_last_file_in_folder_should_update_to_first_file(self):
        self._helper.create_link_at('current folder', last_file=True)

        self._helper.call_from('anywhere with absolute path')

        self._helper.assertThat('update to first file')


    def test_passing_existing_relative_link_name_to_last_file_on_relative_subfolder_should_update_to_first_file(self):
        self._helper.create_link_at('parent folder', last_file=True)

        self._helper.call_from('parent of parent folder')

        self._helper.assertThat('update to first file')

    
    # Autocriation of link when it doesnt exist:

    def test_passing_non_existing_link_in_current_folder_should_create_link_to_first_file_in_folder(self):
        self._helper.call_from('current folder')

        self._helper.assertThat('link was created with first file')

    def test_passing_non_existing_link_in_subfolder_should_create_link_to_first_file_in_folder(self):
        self._helper.call_from('parent folder')

        self._helper.assertThat('link was created with first file')


    def test_passing_non_existing_absolute_link_should_create_link_to_first_file_in_folder(self):
        self._helper.call_from('anywhere with absolute path')

        self._helper.assertThat('link was created with first file')


    # Existing directory validation tests

    def test_passing_a_local_link_name_whose_already_exists_and_is_a_directory_should_raise_error(self):
        self._helper.create_link_at('current folder', as_dir=True)

        self._helper.call_from('current folder', in_context=True)

        self._helper.assertThat('raised parser error')

    def test_passing_a_relative_link_name_whose_already_exists_and_is_a_directory_should_raise_error(self):
        self._helper.create_link_at('current folder', as_dir=True)

        self._helper.call_from('parent folder', in_context=True)

        self._helper.assertThat('raised parser error')


    def test_passing_a_absolute_link_name_whose_already_exists_and_is_a_directory_should_raise_error(self):
        self._helper.create_link_at('current folder', as_dir=True)
    
        self._helper.call_from('anywhere with absolute path', in_context=True)

        self._helper.assertThat('raised parser error')


    # Existing file validation tests

    def test_passing_a_local_link_name_whose_already_exists_and_is_a_file_should_raise_error(self):
        self._helper.create_link_at('current folder', as_file=True)

        self._helper.call_from('current folder', in_context=True)

        self._helper.assertThat('raised parser error')


    def test_passing_a_relative_link_name_whose_already_exists_and_is_a_file_should_raise_error(self):
        self._helper.create_link_at('current folder', as_file=True)

        self._helper.call_from('parent folder', in_context=True)

        self._helper.assertThat('raised parser error')


    def test_passing_a_absolute_link_name_whose_already_exists_and_is_a_file_should_raise_error(self):
        self._helper.create_link_at('current folder', as_file=True)

        self._helper.call_from('anywhere with absolute path', in_context=True)

        self._helper.assertThat('raised parser error')


    # Corner cases:

    # TODO A link which points to a folder with just one file

    def test_passing_non_existing_local_link_name_on_empty_folder_should_raise_error(self):
        self._helper.remove_all_files_on_configs()

        self._helper.call_from('current folder', in_context=True)

        self._helper.assertThat('raised no file to link exception')


    def test_passing_local_broken_link_name_should_update_to_first_file_in_link_folder(self):
        self._helper.create_link_at('current folder', broken_link=True)
    
        self._helper.call_from('current folder')

        self._helper.assertThat('link was created with first file')


    def test_passing_relative_broken_link_name_should_update_to_first_file_in_link_folder(self):
        self._helper.create_link_at('current folder', broken_link=True)
    
        self._helper.call_from('parent folder')

        self._helper.assertThat('link was created with first file')


    def test_passing_absolute_broken_link_name_should_update_to_first_file_in_link_folder(self):
        self._helper.create_link_at('current folder', broken_link=True)
    
        self._helper.call_from('anywhere with absolute path')

        self._helper.assertThat('link was created with first file')


    def test_passing_relative_broken_link_name_to_subfolder_should_update_to_first_file_in_link_folder(self):
        self._helper.enable_logs()

        try:
            self._helper.create_link_at('parent folder', broken_link=True)
    
            self._helper.call_from('parent of parent folder')

            self._helper.assertThat('link was created with first file')
        finally:
            self._helper.disable_logs()


class MockedParserError(Exception):
    
    def __init__(self, message):
        self.message = message


class MockedParser():

    def error(self, message):
        raise MockedParserError(message)



logger = logging.getLogger(__name__)

if __name__ == '__main__':
    import sys

    if len(sys.argv) == 1:
        print('This test case has command line interface, see -h option')

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--conserve-test-folder', '-f', action='store_true', default=False, help='Dont remove test folder after tests')
    parser.add_argument('--clean', '-c', action='store_true', help='Clean test folders')
    parser.add_argument('--skip-breakpoints', '-b', action='store_true', help='Skip breakpoints')

    global args

    args = parser.parse_args()

    _skip_breakpoints = args.skip_breakpoints
    next_file._skip_breakpoints = args.skip_breakpoints

    sys.argv = [sys.argv[0]]

    logging.basicConfig()
    logging.getLogger().setLevel('CRITICAL')

    if args.clean:
        NextFileTests().tearDown()
    else:
        main() 
