#!/usr/bin/env python3


import argparse
import os
import logging


def parse_command_line():
    global parser

    parser = argparse.ArgumentParser(description='Return next file given a link', epilog='In a folder containing "config1", "config2" and "config3" files and a link "active_config" (not necessary in same folder) whose points to "config1" a call passing the link name (relative or absolute name) as parameters will return next files in alphabetical order in link destination folder.')

    parser.add_argument('--loglevel', '-l', default='WARNING', help='Set logger verbosity')
    parser.add_argument('--retrieve-only', '-ro', action='store_true', help='Dont update link, just print new file name')
    parser.add_argument('--pattern', '-p', default='.*', help='File name regex pattern')
    parser.add_argument('link', help='Link to use as base to get next file')

    return parser.parse_args()


class Main(object):

    def __init__(self, link: str, retrieve_only: bool = False, pattern: str = '.*'):
        self._link = link
        import re
        self._pattern = re.compile(pattern)
        self._retrieve_only = retrieve_only


    def run(self):
        logger.debug('Got link = "{}"; retrieve_only = "{}"'.format(self._link, self._retrieve_only))
        logger.debug('Current directory "%s"', os.getcwd())

        self._validate_link()
    
        self._link_dest = self._get_link_dest()
    
        logger.info('Current link destiny: ' + (self._link_dest or '<none>'))
    
        next_file = self._get_next_file()
    
        logger.info('Next file %s', next_file)

        return self._update_link(next_file)


    def _validate_link(self):
        global parser
    
        explanation = None
    
        if os.path.isdir(self._link):
            explanation = 'its a directory'
        elif os.path.isfile(self._link) and not os.path.islink(self._link):
            explanation = 'its a regular file'
    
        if explanation:
            parser.error('"{}" should be a symlink ({})'.format(self._link, explanation))

        logger.info('"{}" is a valid link name'.format(self._link))
 

    def _get_link_dest(self):
        if not os.path.exists(self._link):
            logger.warning('"%s" was not found in filesystem, will be created', self._link)
            return None
    
        link_destiny = os.readlink(self._link)
    
        return os.path.join(os.path.dirname(self._link), link_destiny)
    
    
    def _get_next_file(self):
        files = self._get_file_list()
    
        if not self._link_dest:
            if len(files) == 0:
               raise Exception('Link doesnt exist yet and there are no files to link on')
            logger.warning('"%s" is broken link or doesnt exist, will return to first file in directory', self._link)
            return files[0]
    
        for i, filename in enumerate(files):
            if filename == self._link_dest:
                returned_index = i + 1 if i < len(files) - 1 else 0
                return files[returned_index]
   
        logger.critical('Should never happen! _link_dest: %s; files: %s', self._link_dest, files)


    def _get_file_list(self):
        global _skip_breakpoints
        if not _skip_breakpoints:
            import ipdb
            ipdb.set_trace()
       
        if self._link_dest:
            directory = os.path.dirname(self._link_dest)
        elif self._link:
            if os.path.exists(self._link):
                directory = os.path.dirname(os.readlink(self._link))
            else:
                try:
                    directory = os.path.join(os.path.dirname(self._link), os.path.dirname(os.readlink(self._link)))
                except:
                    directory = os.path.dirname(self._link)
        else:
            directory = ''

        if directory in ['', '.']:
            directory = None

        logger.info('Looking for next file in folder %s', directory or '<none>')

        iterator = os.scandir(directory)
    
        files = list()
    
        link_basename = os.path.basename(self._link)
    
        for entry in iterator:
            logger.debug('"%s" != "%s"', entry.name, link_basename)
            entry_fullpath = os.path.join(directory, entry.name) if directory else entry.name
            if entry.name != link_basename and not entry.is_dir() and self._pattern.match(entry_fullpath):
                files.append(entry_fullpath)
    
        files.sort()
    
        return files


    def _update_link(self, next_file):
        logger.info('Updating link to "%s"', next_file)

        if os.path.exists(self._link) or os.path.islink(self._link):
            old_link_target = os.readlink(self._link)
            logger.debug('"%s" already exists on filesystem (pointing to "%s"), removing it', self._link, old_link_target)
            if not self._retrieve_only:
                os.remove(self._link) 
        else:
            logger.debug('"%s" doesnt exist on filesystem, will point to first file of directory', self._link)
            old_link_target = None


        logger.info('Updating link at "%s" to point to "%s"', self._link, next_file)
        updated_link_dir = os.path.dirname(old_link_target) if old_link_target else ''
        updated_link_name = os.path.basename(next_file)
        updated_link_path = os.path.join(updated_link_dir, updated_link_name)
        logger.debug('Local link destiny "%s"', updated_link_path)
        if not self._retrieve_only:
            os.symlink(updated_link_path, self._link)
        else:
            logger.warning('retrieve-only flag in effect, skip link update')

        return updated_link_path


_skip_breakpoints = True


def main(link: str, retrieve_only: bool, pattern: str = '.*'):
    main = Main(link, retrieve_only, pattern)

    return main.run()


logger = logging.getLogger(__name__)

if __name__ == '__main__':
    args = parse_command_line()

    logging.basicConfig(level=getattr(logging, args.loglevel, None))

    logging.info('Log level set to %s', args.loglevel)

    result = main(args.link, args.retrieve_only, args.pattern)

    print(result)

